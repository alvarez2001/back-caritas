const db = require("../models/index");
const HttpErrorResponse = require("../helpers/http.helpers");

module.exports = {
  /**
   * @route /usuario
   * @headers { "access-token": "token" }
   * @method get
   */
  async getAll(request, response) {
    try {
      const { status, isadmin } = request.query;
      const users = await db.user.getAll(
        status ? status.toString() : "1",
        isadmin ? isadmin.toString() : "0"
      );

      response.status(HttpErrorResponse.OK).json(users);
    } catch (error) {
      console.log(error);
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /usuario/create
   * @headers { "access-token": "token" }
   * @method post
   */
  async create(request, response) {
    try {
      const user = await db.user.CreateModel(request.body);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-create");
    }
  },

  /**
   * @route /usuario/:id
   * @headers { "access-token": "token" }
   * @method get
   */
  async getById(request, response) {
    try {
      const user = await db.user.getById(request.params.id);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /usuario/:id
   * @headers { "access-token": "token" }
   * @method update
   */
  async updateById(request, response) {
    try {
      const data = request.body;
      const userUpdate = await db.user.updateById(request.params.id, data);
      const user = await db.user.getById(request.params.id);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /usuario/:id/:where
   * @headers { "access-token": "token" }
   * @method patch
   */
  async updateStatusAndAdmin(request, response) {
    try {
      const { id, where } = request.params;
      const userUpdate = await db.user.updateStatusAndAdmin(id, {
        [where]: request.body.value,
      });

      const user = await db.user.getById(id);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /usuario/:id
   * @headers { "access-token": "token" }
   * @method delete
   */
  async deleteById(request, response) {
    try {
      const user = await db.user.deleteById(request.params.id);

      response.status(HttpErrorResponse.OK).json({
        message: "Usuario eliminado correctamente",
        status: true,
      });
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },
};

const db = require("../models/index");
const HttpErrorResponse = require("../helpers/http.helpers");

module.exports = {
  /**
   * @route /proyecto
   * @headers { "access-token": "token" }
   * @method get
   */
  async getAll(request, response) {
    try {
      const users = await db.proyecto.getAll();

      response.status(HttpErrorResponse.OK).json(users);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /proyecto/create
   * @headers { "access-token": "token" }
   * @method post
   */
  async create(request, response) {
    try {
      const user = await db.proyecto.CreateModel(request.body);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-create");
    }
  },

  /**
   * @route /proyecto/:id
   * @headers { "access-token": "token" }
   * @method get
   */
  async getById(request, response) {
    try {
      const user = await db.proyecto.getById(request.params.id);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /proyecto/:id
   * @headers { "access-token": "token" }
   * @method update
   */
  async updateById(request, response) {
    try {
      const data = request.body;
      const userUpdate = await db.proyecto.updateById(request.params.id, data);
      const user = await db.proyecto.getById(request.params.id);

      response.status(HttpErrorResponse.OK).json(user);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-update");
    }
  },

  /**
   * @route /proyecto/:id
   * @headers { "access-token": "token" }
   * @method delete
   */
  async deleteById(request, response) {
    try {
      const user = await db.proyecto.deleteById(request.params.id);

      response.status(HttpErrorResponse.OK).json({
        message: "Proyecto eliminado correctamente",
        status: true,
      });
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-delete");
    }
  },
};

const db = require("../models/index");
const HttpErrorResponse = require("../helpers/http.helpers");

module.exports = {
  /**
   * @route /banco
   * @headers { "access-token": "token" }
   * @method get
   */
  async getAll(request, response) {
    try {
      const bancos = await db.banco.getAll(db.proyecto);

      response.status(HttpErrorResponse.OK).json(bancos);
    } catch (error) {
      console.log(error);
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-all");
    }
  },

  /**
   * @route /banco/create
   * @headers { "access-token": "token" }
   * @method post
   */
  async create(request, response) {
    try {
      const banco = await db.banco.CreateModel(request.body);

      if (request.body.create_proyecto && request.body.proyecto) {
        const proyecto = await db.proyecto.CreateModel(request.body.proyecto);
        await db.banco.setProyectos(proyecto);
        banco.proyectos = [proyecto];
      }

      response.status(HttpErrorResponse.OK).json(banco);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-create");
    }
  },

  /**
   * @route /banco/:id
   * @headers { "access-token": "token" }
   * @method get
   */
  async getById(request, response) {
    try {
      const banco = await db.banco.getById(request.params.id, db.proyecto);

      response.status(HttpErrorResponse.OK).json(banco);
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-get");
    }
  },

  /**
   * @route /banco/:id
   * @headers { "access-token": "token" }
   * @method update
   */
  async updateById(request, response) {
    try {
      const data = request.body;
      const bancoUpdate = await db.banco.updateById(request.params.id, data);
      const banco = await db.banco.getById(request.params.id, db.proyecto);

      response.status(HttpErrorResponse.OK).json(banco);
    } catch (error) {
      console.log(error);
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-update");
    }
  },

  /**
   * @route /banco/:id
   * @headers { "access-token": "token" }
   * @method delete
   */
  async deleteById(request, response) {
    try {
      const banco = await db.banco.deleteById(request.params.id);

      response.status(HttpErrorResponse.OK).json({
        message: "Proyecto eliminado correctamente",
        status: true,
      });
    } catch (error) {
      response.status(HttpErrorResponse.BAD_REQUEST).json("error-delete");
    }
  },
};

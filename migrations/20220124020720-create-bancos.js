'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('bancos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titular: {
        type: Sequelize.STRING
      },
      rif: {
        type: Sequelize.STRING
      },
      banco: {
        type: Sequelize.STRING
      },
      alias: {
        type: Sequelize.STRING
      },
      numero_cuenta: {
        type: Sequelize.STRING
      },
      clasificacion_cuenta: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('bancos');
  }
};
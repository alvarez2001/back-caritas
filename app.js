const express = require("express");
const cors = require("cors");
require("dotenv").config();

const db = require("./models/index");

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use("/v1", require("./router/index.router"));

app.listen(process.env.PORT, () => {
  db.sequelize
    .sync({ force:false, logging:false })
    .then(() => {
      console.log("Conexion establecia a la Base de Datos");
    })
    .catch((err) => {
      console.log(err);
    });

  console.log(`Server is running on port ${process.env.PORT}`);
});

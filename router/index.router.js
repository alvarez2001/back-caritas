const express = require("express")();

const fs = require("fs");

fs.readdirSync(__dirname).forEach((file) => {
  const fileName = file.split(".")[0];
  if (fileName !== "index") {
    const arr = file.split(".");
    arr.pop();
    const join = arr.join(".");
    express.use(`/${fileName}`, require(`./${join}`));
  }
});

module.exports = express;

const router = require("express").Router();

const controller = require("../controllers/usuario.controller");



/**
 * @route /usuario
 * @headers { "access-token": "token" }
 * @method get
 */
 router.get("/", controller.getAll);

/**
 * @route /usuario/create
 * @headers { "access-token": "token" }
 * @method post
 */
router.post("/create", controller.create);

/**
 * @route /usuario/:id
 * @headers { "access-token": "token" }
 * @method get
 */
router.get("/:id", controller.getById);

/**
 * @route /usuario/:id
 * @headers { "access-token": "token" }
 * @method update
 */
router.put("/:id", controller.updateById);

/**
 * @route /usuario/:id/:where
 * @headers { "access-token": "token" }
 * @method patch
 */
router.patch("/:id/:where", controller.updateStatusAndAdmin);

/**
 * @route /usuario/:id
 * @headers { "access-token": "token" }
 * @method delete
 */
router.delete("/:id", controller.deleteById);

module.exports = router;

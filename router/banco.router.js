const router = require("express").Router();

const controller = require("../controllers/banco.controller");

/**
 * @route /banco
 * @headers { "access-token": "token" }
 * @method get
 */
router.get("/", controller.getAll);

/**
 * @route /banco/create
 * @headers { "access-token": "token" }
 * @method post
 */
router.post("/create", controller.create);

/**
 * @route /banco/:id
 * @headers { "access-token": "token" }
 * @method get
 */
router.get("/:id", controller.getById);

/**
 * @route /banco/:id
 * @headers { "access-token": "token" }
 * @method update
 */
router.put("/:id", controller.updateById);

/**
 * @route /banco/:id
 * @headers { "access-token": "token" }
 * @method delete
 */
router.delete("/:id", controller.deleteById);

module.exports = router;

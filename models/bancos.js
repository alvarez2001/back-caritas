"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Banco extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Banco.belongsToMany(models.proyecto, {
        as: "proyectos",
        through: "banco_proyecto",
      });
    }

    static CreateModel(data) {
      return Banco.create(data, {
        fields: [
          "titular",
          "rif",
          "banco",
          "alias",
          "numero_cuenta",
          "clasificacion_cuenta",
        ],
      });
    }

    static getById(id, proyecto) {
      return Banco.findOne({
        where: {
          id: id,
        },
        attributes: {
          exclude: ["deletedAt", "createdAt", "updatedAt"],
        },
        include: [
          {
            model: proyecto,
            as: "proyectos",
          },
        ],
      });
    }

    static getAll(proyecto) {
      return Banco.findAll({
        attributes: {
          exclude: ["deletedAt", "updatedAt"],
        },
        include: [
          {
            model:proyecto,
            as: "proyectos",
          },
        ],
      });
    }

    static deleteById(id) {
      return Banco.destroy({
        where: {
          id: id,
        },
      });
    }

    static updateById(id, data) {
      return Banco.update(data, {
        where: {
          id,
        },
        fields: [
          "titular",
          "rif",
          "banco",
          "alias",
          "numero_cuenta",
          "clasificacion_cuenta",
        ],
      });
    }
  }
  Banco.init(
    {
      titular: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      rif: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      banco: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      alias: {
        type: DataTypes.STRING,
      },
      numero_cuenta: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      clasificacion_cuenta: {
        type: DataTypes.ENUM,
        values: ["personal", "proveedor"],
        defaultValue: "personal",
      },
    },
    {
      sequelize,
      paranoid: true,
      modelName: "banco",
    }
  );
  return Banco;
};

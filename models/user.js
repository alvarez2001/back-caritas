"use strict";
const { Model } = require("sequelize");
const { hashSync } = require("bcryptjs");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }


    static CreateModel(data) {
      const password = hashSync(data.password ? data.password : "", 10);
      data.password = password;
      return User.create(data);
    }

    static getById(id) {
      return User.findOne({
        where: {
          id: id,
        },
        attributes: {
          exclude: ["deletedAt", "createdAt", "updatedAt", "password"],
        },
      });
    }

    static deleteById(id) {
      return User.destroy({
        where: {
          id: id,
        },
      });
    }

    static getAll(status = "1", isadmin = "0") {
      return User.findAll({
        where: {
          status,
          isadmin,
        },
        attributes: {
          exclude: ["password", "deletedAt", "updatedAt"],
        },
      });
    }

    static updateById(id, data) {
      return User.update(data, {
        where: {
          id,
        },
        fields: ["nombre", "cedula"],
      });
    }

    static updateStatusAndAdmin(id, data) {
      return User.update(data, {
        where: {
          id,
        },
        fields: ["status", "isadmin"],
      });
    }
  }
  User.init(
    {
      nombre: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cedula: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      isadmin: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      paranoid: true,
      modelName: "user",
    }
  );
  return User;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Proyecto extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Proyecto.belongsToMany(models.banco, {
        as: "bancos",
        through: "banco_proyecto",
      });
    }

    static CreateModel(data) {
      return Proyecto.create(data);
    }

    static getById(id) {
      return Proyecto.findOne({
        where: {
          id: id,
        },
        attributes: {
          exclude: ["deletedAt", "createdAt", "updatedAt"],
        },
      });
    }

    static getAll() {
      return Proyecto.findAll({
        attributes: {
          exclude: ["deletedAt", "updatedAt"],
        },
      });
    }
    ////

    static deleteById(id) {
      return Proyecto.destroy({
        where: {
          id: id,
        },
      });
    }

    static updateById(id, data) {
      return Proyecto.update(data, {
        where: {
          id,
        },
        fields: [
          "nombre",
          "fecha_inicio",
          "fecha_culminacion",
          "socio",
          "monto_financiamiento",
          "moneda",
          "alias",
        ],
      });
    }

    ///
  }
  Proyecto.init(
    {
      nombre: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      fecha_inicio: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      fecha_culminacion: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      socio: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      monto_financiamiento: {
        type: DataTypes.DECIMAL(16, 2),
        allowNull: false,
      },
      moneda: {
        type: DataTypes.ENUM,
        values: ["dolar", "euro"],
        defaultValue: "euro",
        allowNull: false,
      },
      alias: {
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      paranoid: true,
      modelName: "proyecto",
    }
  );
  return Proyecto;
};
